import datetime
import json
import logging

from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS

from django.views.decorators.csrf import csrf_exempt
from drf_yasg.utils import swagger_auto_schema
# from api.utils import send_telegram, check_holidays
from rest_framework.decorators import api_view
from rest_framework.generics import GenericAPIView, ListCreateAPIView, UpdateAPIView, ListAPIView, DestroyAPIView, CreateAPIView
import requests
import jwt

from drf import settings
from gitlab.models import Project, Users, Merges
from gitlab.serializers import (
    ProjectGetSerializer,
    ProjectPostSerializer,
    UsersGetSerializer,
    UsersSetTimeSerializer,
)

from rest_framework.response import Response

logger = logging.getLogger(__name__)

requests.get(
    "https://api.telegram.org/bot"
    + str(settings.TELEGRAM_TOKEN)
    + "/setWebhook?url=https://"
    + str(settings.ALLOWED_HOSTS[1])
    + "/api/v1/tg_hook/"
)

header = {"content-type": "application/json"}
data = {
    "id": "1",
    "merge_requests_events": True,
    "url": f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/",
}
for projects in Project.objects.all():
    url = f"{settings.GIT_LAB_API_BASE_URL}{projects.project_id}/hooks?access_token={projects.token}"  # noqa
    get = requests.get(url)
    j = json.loads(get.text)
    need_hook = True
    for hook in j:
        if hook != "message":
            if hook["url"] == f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/":
                need_hook = False
    if need_hook:
        requests.post(url, params=data, headers=header)

class JWT_chek(BasePermission):
    def has_permission(self, request, view):
        token = request.META.get("HTTP_AUTHORIZATION", " ")
        result = check_permition(token)
        if not result:
            return False
        else:
            return True

class ProjectList(ListCreateAPIView):
    """
    get:Получение проектов админа
    post:регистрация нового пректа
    """
    serializer_class = ProjectPostSerializer
    permission_classes = [JWT_chek]


    def get_queryset(self):
        logger.warning('qwe')
        token = self.request.META.get("HTTP_AUTHORIZATION", " ")
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

        return Project.objects.filter(author=payload_decoded["user_id"])

    def create(self, request):
        token = request.META.get("HTTP_AUTHORIZATION", " ")
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])

        review = ProjectPostSerializer(data=request.data)
        if review.is_valid():
            try:
                creator_id = requests.get(
                    f"{settings.GIT_LAB_API_BASE_URL}"
                    f"{review.data.get('project_id')}"
                    f"?access_token={review.data.get('token')}"
                ).json()["creator_id"]

                users = requests.get(
                    f"{settings.GIT_LAB_API_BASE_URL}"
                    f"{review.data.get('project_id')}"
                    f"/users?access_token={review.data.get('token')}"
                ).json()

                proj = Project(
                    project_id=review.data.get("project_id"),
                    author=payload_decoded["user_id"],
                    token=review.data.get("token"),
                )
                proj.save()

                persons = []
                for user in users:
                    if user["id"] != creator_id:
                        persons.append(user)
                for list in persons:
                    git_users = Users(
                        gitlab_name=list["name"], status=True, work_time=None
                    )
                    git_users.save()
                    git_users.project.add(proj)
            except KeyError:
                return Response({"error": "incorrect data"})

        proj = Project.objects.filter(author=payload_decoded["user_id"])
        serializer = ProjectGetSerializer(proj, many=True)

        header = {"content-type": "application/json"}
        data = {
            "id": "1",
            "merge_requests_events": True,
            "url": f"https://{settings.ALLOWED_HOSTS[1]}/api/v1/git_hook/",
        }
        url = f"{settings.GIT_LAB_API_BASE_URL}{review.data.get('project_id')}/hooks?access_token={review.data.get('token')}"  # noqa
        get = requests.get(url)
        j = json.loads(get.text)
        need = True
        if len(j) == 0:
            requests.post(url, params=data, headers=header)
        else:
            for actions in j:
                if actions["merge_requests_events"]:
                    need = False
        if need:
            requests.post(url, params=data, headers=header)

        return Response(serializer.data)


class UpdateToken(UpdateAPIView):
    """
    put:Обновить токен проекта
    """

    serializer_class = ProjectPostSerializer
    permission_classes = [JWT_chek]

    def update(self, request):
        project = Project.objects.filter(project_id=request.data.get("project_id"))
        if len(project) != 0:
            project.update(token=request.data.get("token"))
            return Response({"message": "Success"})
        return Response({"message": "Fail"})


class UsersList(ListAPIView):
    """
    get:Список всех пользователей
    """
    serializer_class = UsersGetSerializer
    permission_classes = [JWT_chek]
    queryset = Users.objects.all()


class UsersChange(UpdateAPIView,DestroyAPIView):
    """
    put:Установить выходной проверяющему
    delete:Удалить пользователя
    """
    serializer_class = UsersSetTimeSerializer
    permission_classes = [JWT_chek]


    def patch(self,request):
        return Response(status=status.HTTP_409_CONFLICT)

    def update(self, request,id):
        Users.objects.filter(id=id).update(
            work_time=request.data.get("work_time"), status=False
        )
        users = Users.objects.all()
        serializer = UsersGetSerializer(users, many=True)
        return Response(serializer.data)

    def destroy(self, request, id):
        Users.objects.filter(id=id).delete()
        users = Users.objects.all()
        serializer = UsersGetSerializer(users, many=True)
        return Response(serializer.data)


def check_permition(token):
    try:
        jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        result = True
    except:  # noqa
        result = False
    return result


def check_holidays(project_id):
    time = datetime.date.today()
    project = Project.objects.filter(project_id=project_id).get()
    for user in Users.objects.filter(project=project):
        need_change = True
        for merge in Merges.objects.filter(reviewer_gitlab=user.gitlab_name):
            if merge.status:
                need_change = False
        if user.work_time is not None and need_change:
            if user.work_time <= time and not user.status:
                Users.objects.filter(id=user.id).update(status=True, work_time=None)
        elif need_change:
            if not user.status:
                Users.objects.filter(id=user.id).update(status=True, work_time=None)


def send_telegram(user_tg_id, text):
    token = settings.TELEGRAM_TOKEN

    url = "https://api.telegram.org/bot"
    user_id = user_tg_id
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={"chat_id": user_id, "text": text})

    if r.status_code != 200:
        raise Exception("post_text error")


@swagger_auto_schema(method="post", auto_schema=None)
@api_view(["POST"])
@csrf_exempt
def getMerge(request):
    j = json.loads(request.body)
    username = j["user"]["name"]
    project_id = j["project"]["id"]
    merge_id = j["object_attributes"]["iid"]  # merge id
    action = j["object_attributes"]["action"]  # unapproved,reopen,open
    url = j["object_attributes"]["url"]
    check_holidays(project_id)
    if action == "open":
        user = (
            Project.objects.filter(project_id=project_id)[0]
            .users_set.filter(status=True)
            .exclude(gitlab_name=username)[0]
        )
        Merges(
            author_gitlab=username,
            reviewer_gitlab=user.gitlab_name,
            merge_id=merge_id,
            project=project_id,
        ).save()
        Users.objects.filter(gitlab_name=user.gitlab_name).update(status=False)
        send_telegram(
            user.telegram_id, "Проверте пожалуйста этот Merge Request \n" + str(url)
        )
    if action == "approved":
        merge = Merges.objects.get(merge_id=merge_id)
        if username == merge.reviewer_gitlab:
            Merges.objects.filter(merge_id=merge_id).update(status=False)
            Users.objects.filter(gitlab_name=username).update(status=True)
    if action == "merge":
        merge = Merges.objects.filter(merge_id=merge_id)
        merge.update(status=False)
        Users.objects.filter(gitlab_name=merge.reviewer_gitlab).update(status=True)


@csrf_exempt
@swagger_auto_schema(method="post", auto_schema=None)
@api_view(("POST",))
def getTelegram(request):
    j = json.loads(request.body)
    id = j["message"]["from"]["id"]
    message = j["message"]["text"]
    if not message.startswith("#"):
        send_telegram(id, "Введите своё имя в GitLab (формат - #Имя)")
    elif message.startswith("#"):
        name = message.split("#")[1]
        if Users.objects.filter(gitlab_name=name).exists():
            Users.objects.filter(gitlab_name=name).update(telegram_id=id, status=True)
            send_telegram(id, "Успешно добавлено")
        else:
            send_telegram(id, "Имя не существует,попробуйте ещё раз")
    return Response({"": ""})